package io.adminboot.quartz.service.impl;

import io.adminboot.quartz.domain.JobInputDTO;
import io.adminboot.quartz.domain.JobOutputDTO;
import io.adminboot.quartz.service.IQuartzService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JobServiceImplTest {

    @Autowired
    @Qualifier("quartzService")
    private IQuartzService quartzService;

    @Test
    public void add() {
        JobInputDTO dto = new JobInputDTO();
        dto.setJobName("junitTest");
        dto.setGroupName("junitGroup");
        dto.setDescp("单元测试");
        dto.setJobClassName("io.adminboot.quartz.job.TestJob");
        // 从1分钟开始,每3分钟执行一次
        dto.setCronExpression("0 1/3 * * * ? ");
        quartzService.add(dto);
    }

    @Test
    public void trigger() {
        quartzService.trigger("junitTest", "junitGroup");
    }

    @Test
    public void pause() {
        quartzService.pause("testJob01", "testGroup");
    }

    @Test
    public void resume() {
        quartzService.resume("testJob01", "testGroup");
    }

    @Test
    public void remove() {
        quartzService.remove("junitTest", "junitGroup");
    }

    @Test
    public void list() {
        final List<JobOutputDTO> list = quartzService.list(null);
        list.stream().forEach(System.out::println);
    }

    @Test
    public void count() {
        final Long count = quartzService.count();
        System.out.println("已配置任务总数量：" + count);
    }
}