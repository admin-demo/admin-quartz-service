package io.adminboot.quartz.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.Serializable;
import java.util.Date;

public class ChickenJob implements  Job,Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		Date now = new Date(System.currentTimeMillis());

		System.out.println("大吉大利、今晚吃鸡:" + now);
	}
}
