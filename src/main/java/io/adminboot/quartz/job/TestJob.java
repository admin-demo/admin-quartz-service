package io.adminboot.quartz.job;

import io.adminboot.quartz.service.IQuartzService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Date;

/**
 * 实现序列化接口、防止重启应用出现quartz Couldn't retrieve job because a required class was not found 的问题
 */
public class TestJob implements  Job,Serializable {

	private static final long serialVersionUID = 1L;
	   
    @Autowired
    private IQuartzService quartzService;
    
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println(quartzService);//注入jobService 执行相关业务操作
		Date now = new Date();
		System.out.println("任务执行成功:" + now);
	}
}
