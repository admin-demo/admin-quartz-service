package io.adminboot.quartz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan({"io.adminboot.quartz.repository"})
@SpringBootApplication
public class AdminbootQuartzApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminbootQuartzApplication.class, args);
	}
}
