package io.adminboot.quartz.exception;

/**
 * Job服务异常
 */
public class JobServiceException extends RuntimeException {

    public JobServiceException(String message) {
        super(message);
    }

    public JobServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
