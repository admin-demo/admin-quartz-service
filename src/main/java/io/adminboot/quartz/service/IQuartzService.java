package io.adminboot.quartz.service;


import io.adminboot.quartz.domain.JobInputDTO;
import io.adminboot.quartz.domain.JobOutputDTO;
import io.adminboot.quartz.exception.JobServiceException;

import java.util.List;

public interface IQuartzService {

    /**
     * 新增任务
     *
     * @param inputDTO@throws JobServiceException
     */
    void add(JobInputDTO inputDTO) throws JobServiceException;

    /**
     * 触发任务（立即执行）
     * @param jobName
     * @param groupName
     */
    void trigger(String jobName, String groupName);

    /**
     * 暂停任务
     * @param jobName
     * @param groupName
     */
    void pause(String jobName, String groupName);

    /**
     * 恢复任务
     * @param jobName
     * @param groupName
     */
    void resume(String jobName, String groupName);

    /**
     * 移除任务
     * @param jobName
     * @param groupName
     */
    void remove(String jobName, String groupName);

    /**
     * 任务列表
     * @param jobName
     * @return
     */
    List<JobOutputDTO> list(String jobName);


    Long count();
}
