package io.adminboot.quartz.repository;

import io.adminboot.quartz.entity.QuartzEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobMapper {

    List<QuartzEntity> selectList(@Param("name") String name);

    Long count();

}
