package io.adminboot.quartz.domain;

public class JobInputDTO {

    /**
     * 任务名称
     */
    private String jobName;
    /**
     * 任务组别名称
     */
    private String groupName;
    /**
     * 任务className
     */
    private String jobClassName;
    /**
     * 任务表cron达式
     */
    private String cronExpression;
    /**
     * 任务描述
     */
    private String descp;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getJobClassName() {
        return jobClassName;
    }

    public void setJobClassName(String jobClassName) {
        this.jobClassName = jobClassName;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }
}
