package io.adminboot.quartz.domain;

import io.adminboot.quartz.entity.QuartzEntity;

import java.util.Objects;

public class JobOutputDTO {

    /**
     * 任务名称
     */
    private String jobName;
    /**
     * 任务分组
     */
    private String jobGroup;
    /**
     * 任务描述
     */
    private String description;
    /**
     * 执行类
     */
    private String jobClassName;
    /**
     * 执行时间
     */
    private String cronExpression;
    /**
     * 执行时间
     */
    private String triggerName;
    /**
     * 任务状态
     */
    private String triggerState;

    public JobOutputDTO() {
    }

    public JobOutputDTO(QuartzEntity quartzEntity) {
        this.jobName = quartzEntity.getJobName();
        this.jobGroup = quartzEntity.getJobGroup();
        this.jobClassName = quartzEntity.getJobClassName();
        this.description = quartzEntity.getDescription();
        this.cronExpression = quartzEntity.getCronExpression();
        this.triggerName = quartzEntity.getTriggerName();
        this.triggerState = quartzEntity.getTriggerState();
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobClassName() {
        return jobClassName;
    }

    public void setJobClassName(String jobClassName) {
        this.jobClassName = jobClassName;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public String getTriggerState() {
        return triggerState;
    }

    public void setTriggerState(String triggerState) {
        this.triggerState = triggerState;
    }

    @Override
    public String toString() {
        return "JobOutputDTO{" +
                "jobName='" + jobName + '\'' +
                ", jobGroup='" + jobGroup + '\'' +
                ", description='" + description + '\'' +
                ", jobClassName='" + jobClassName + '\'' +
                ", cronExpression='" + cronExpression + '\'' +
                ", triggerName='" + triggerName + '\'' +
                ", triggerState='" + triggerState + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobOutputDTO that = (JobOutputDTO) o;
        return Objects.equals(jobName, that.jobName) &&
                Objects.equals(jobGroup, that.jobGroup) &&
                Objects.equals(description, that.description) &&
                Objects.equals(jobClassName, that.jobClassName) &&
                Objects.equals(cronExpression, that.cronExpression) &&
                Objects.equals(triggerName, that.triggerName) &&
                Objects.equals(triggerState, that.triggerState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(jobName, jobGroup, description, jobClassName, cronExpression, triggerName, triggerState);
    }
}
